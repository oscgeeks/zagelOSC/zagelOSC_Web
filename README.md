# zagelOSC_Web

Hi! This is teh zagelOSC_Web project. It is built using laravel.

## Dependencies

* [composer](https://getcomposer.org)
* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension

## Installation
To get the project up and running, follow the following instructions:

* `git clone https://gitlab.com/oscgeeks/zagelOSC/zagelOSC_Web.git`
* `cd zagelOSC_Web`
* `composer install`
* `cp .env.example .env`
* Add your database info into the .env file (assuming you created a database for the project)
* `php artisan key:generate`
* `php artisan migrate`
* if you wanna seed the database with dummy data: `php artisan db:seed`
* `php artisan passport:install`

and to run a development server
* `php artisan serve`