
function $(id) {
	return document.getElementById(id);
}

function properlyTextAlign(elem) {
	var renderedDirection = window.getComputedStyle(elem, null).direction;

	if (renderedDirection == 'rtl') {
		elem.style.textAlign = "right";
	} else {
		elem.style.textAlign = "left";
	}
}
