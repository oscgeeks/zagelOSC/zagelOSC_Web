@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card">
                    <div class="card-header">Post</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="dropdown" style="float:right">
                                                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                @if(Auth::user()->id == $post->user->id)
                                                <a class="dropdown-item" href="/posts/{{$post->id}}/edit">Edit Post</a>
                                                @endif

                                                @if(Auth::user()->id == $post->user->id)
                                                <a class="dropdown-item" href="/posts/{{$post->id}}/delete">Delete Post</a>
                                                @endif

                                                <a class="dropdown-item" href="#">Report to Admin</a>
                                                </div>
                                            </div>
                                            <a href="/users/{{$post->user->id}}" >{{ $post->user->name }}</a>
                                            <br>
                                            {{ $post->user->created_at->diffForHumans() }}
                                        </div>
                                        <div class="card-body" dir="auto" id="{{ $post->id }}">
                                            <p class="card-text">
                                            {!! nl2br(e($post->content)) !!}
                                            @if($post->image_path() != null)
                                                <br/>
                                                <img src="{{ $post->image_path() }}" style="max-width: 100%">
                                            @endif
                                            </p>
                                        </div>
                                        <script type="text/javascript">
                                            properlyTextAlign($("{{ $post->id }}"));
                                        </script>
                                        <div class="card-footer">

                                            <div class="input-group">
                                                <a class="col-sm-3" href="/posts/{{ $post->id }}">
                                                Add Comment</a>

                                                <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                @csrf
                                                {{ $post->getReactionsArray()['love'] }}
                                                <input type="hidden" name="reaction_type" value="love">
                                                <input type="image" src="/images/reactions/love.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                @csrf
                                                {{ $post->getReactionsArray()['lol'] }}
                                                <input type="hidden" name="reaction_type" value="lol">
                                                <input type="image" src="/images/reactions/lol.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                @csrf
                                                {{ $post->getReactionsArray()['angry'] }}
                                                <input type="hidden" name="reaction_type" value="angry">
                                                <input type="image" src="/images/reactions/angry.png" width="24" height="24"/>
                                                </form>

                                                

                                                <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                @csrf
                                                {{ $post->getReactionsArray()['wow'] }}
                                                <input type="hidden" name="reaction_type" value="wow">
                                                <input type="image" src="/images/reactions/wow.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                @csrf
                                                {{ $post->getReactionsArray()['sad'] }}
                                                <input type="hidden" name="reaction_type" value="sad">
                                                <input type="image" src="/images/reactions/sad.png" width="24" height="24"/>
                                                </form>

                                                @if(count($post->comments) > 0)
                                                <a class="col-sm-3" href="/posts/{{ $post->id }}">
                                                    {{ count($post->comments) }} Comments</a>
                                                @endif
                                            </div>
                                            
                                            <p class="card-text">{{ count($post->comments) }} Comments</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        @foreach ($post->comments as $comment)
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="dropdown" style="float:right">
                                                    <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button"
                                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    @if(Auth::user()->id == $comment->user->id)
                                                    <a class="dropdown-item" href="/comments/{{$comment->id}}/edit">Edit Comment</a>
                                                    @endif

                                                    @if(Auth::user()->id == $comment->user->id)
                                                    <a class="dropdown-item" href="/comments/{{$comment->id}}/delete">Delete Comment</a>
                                                    @endif

                                                    <a class="dropdown-item" href="#">Report to Admin</a>
                                                    </div>
                                                </div>
                                                <a href="/users/{{$comment->user->id}}" style="float:left">{{ $comment->user->name }}</a>

                                                <br>
                                                @if($comment->user->created_at->diffInDays() > 7)
                                                {{ $comment->user->created_at->format('d M Y, h:m') }}
                                                @else
                                                {{ $comment->user->created_at->diffForHumans() }}
                                                @endif
                                                
                                            </div>
                                            <div class="card-body" dir="auto" id="{{ $comment->id }}">
                                                <p class="card-text">
                                                {!! nl2br(e($comment->content)) !!}
                                                @if($comment->image_path() != null)
                                                    <br/>
                                                    <img src="{{ $comment->image_path() }}" style="max-width: 100%">
                                                @endif
                                                </p>
                                            </div>
                                            <script type="text/javascript">
                                                properlyTextAlign($("{{ $comment->id }}"));
                                            </script>
                                            <div class="card-footer">

                                            <div class="input-group">

                                                <form class="col-sm-1" method="POST" action="/comments/{{$comment->id}}/react">
                                                @csrf
                                                {{ $comment->getReactionsArray()['love'] }}
                                                <input type="hidden" name="reaction_type" value="love">
                                                <input type="image" src="/images/reactions/love.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/comments/{{$comment->id}}/react">
                                                @csrf
                                                {{ $comment->getReactionsArray()['lol'] }}
                                                <input type="hidden" name="reaction_type" value="lol">
                                                <input type="image" src="/images/reactions/lol.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/comments/{{$comment->id}}/react">
                                                @csrf
                                                {{ $comment->getReactionsArray()['angry'] }}
                                                <input type="hidden" name="reaction_type" value="angry">
                                                <input type="image" src="/images/reactions/angry.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/comments/{{$comment->id}}/react">
                                                @csrf
                                                {{ $comment->getReactionsArray()['wow'] }}
                                                <input type="hidden" name="reaction_type" value="wow">
                                                <input type="image" src="/images/reactions/wow.png" width="24" height="24"/>
                                                </form>

                                                <form class="col-sm-1" method="POST" action="/comments/{{$comment->id}}/react">
                                                @csrf
                                                {{ $comment->getReactionsArray()['sad'] }}
                                                <input type="hidden" name="reaction_type" value="sad">
                                                <input type="image" src="/images/reactions/sad.png" width="24" height="24"/>
                                                </form>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        @endforeach

                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Write a comment
                                        </div>
                                        <form method="POST" action="/comments" enctype="multipart/form-data">
                                            @csrf
                                            <div class="card-body">

                                                <div class="form-group">
                                                    <textarea placeholder="Wite a comment" class="form-control" rows="5" name="content" id="text-input" dir="auto" required></textarea>
                                                    <input type="hidden" name="post_id" value="{{$post->id}}"></input>
                                                </div>
                                                <div class="form-group">
                                                    <label>Select an image to upload:</label>
                                                    <input type="file" name="image" accept="image/*"/>
                                                </div>

                                            </div>
                                            <div class="card-footer">
                                                <button class="btn btn-primary">Comment</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
