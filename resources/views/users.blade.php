@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Users
                                </div>
                                <div class="card-body">
                                    @for ($i = 0; $i < count($users); $i++)
                                        <p>{{$i+1}}) <a href="/users/{{$users[$i]->id}}"
                                                        class="card-text">{{$users[$i]->name}}</a></p>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
