@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">News Feed</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Write a post
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form method="POST" action="/posts" enctype="multipart/form-data">
                                            @csrf
                                            <div class="card-body">

                                                <div class="form-group">
                                                    <textarea placeholder="Write a post" class="form-control" rows="5" id="text-input" dir="auto" name="content" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Select an image to upload:</label>
                                                    <input type="file" name="image" accept="image/*"/>
                                                </div>

                                            </div>
                                            <div class="card-footer">
                                                <button class="btn btn-primary">Post</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        @foreach ($posts as $post)
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="dropdown" style="float:right">
                                                    <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button"
                                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    @if(Auth::user()->id == $post->user->id)
                                                    <a class="dropdown-item" href="/posts/{{$post->id}}/edit">Edit Post</a>
                                                    @endif

                                                    @if(Auth::user()->id == $post->user->id)
                                                    <a class="dropdown-item" href="/posts/{{$post->id}}/delete">Delete Post</a>
                                                    @endif

                                                    <a class="dropdown-item" href="#">Report to Admin</a>
                                                    </div>
                                                </div>
                                                <a href="/users/{{$post->user->id}}">{{ $post->user->name }}</a>
                                                <br>
                                                @if($post->user->created_at->diffInDays() > 7)
                                                {{ $post->user->created_at->format('d M Y, h:m') }}
                                                @else
                                                {{ $post->user->created_at->diffForHumans() }}
                                                @endif
                                                
                                            </div>
                                            <div class="card-body" dir="auto" id="{{$post->id}}">
                                                {!! nl2br(e($post->content)) !!}
                                                @if($post->image_path() != null)
                                                    <br/>
                                                    <img src="{{ $post->image_path() }}" style="max-width: 100%" />
                                                @endif
                                            </div>
                                                <script type="text/javascript">
                                                    properlyTextAlign($("{{$post->id}}"));
                                                </script>
                                                <div class="card-footer">

                                                    <div class="input-group">
                                                        <a class="col-sm-3" href="/posts/{{ $post->id }}">
                                                        Add Comment</a>

                                                        <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                        @csrf
                                                        {{ $post->getReactionsArray()['love'] }}
                                                        <input type="hidden" name="reaction_type" value="love">
                                                        <input type="image" src="/images/reactions/love.png" width="24" height="24"/>
                                                        </form>

                                                        <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                        @csrf
                                                        {{ $post->getReactionsArray()['lol'] }}
                                                        <input type="hidden" name="reaction_type" value="lol">
                                                        <input type="image" src="/images/reactions/lol.png" width="24" height="24"/>
                                                        </form>

                                                        <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                        @csrf
                                                        {{ $post->getReactionsArray()['angry'] }}
                                                        <input type="hidden" name="reaction_type" value="angry">
                                                        <input type="image" src="/images/reactions/angry.png" width="24" height="24"/>
                                                        </form>

                                                        

                                                        <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                        @csrf
                                                        {{ $post->getReactionsArray()['wow'] }}
                                                        <input type="hidden" name="reaction_type" value="wow">
                                                        <input type="image" src="/images/reactions/wow.png" width="24" height="24"/>
                                                        </form>

                                                        <form class="col-sm-1" method="POST" action="/posts/{{$post->id}}/react">
                                                        @csrf
                                                        {{ $post->getReactionsArray()['sad'] }}
                                                        <input type="hidden" name="reaction_type" value="sad">
                                                        <input type="image" src="/images/reactions/sad.png" width="24" height="24"/>
                                                        </form>

                                                        @if(count($post->comments) > 0)
                                                        <a class="col-sm-3" href="/posts/{{ $post->id }}">
                                                            {{ count($post->comments) }} Comments</a>
                                                        @endif
                                                    </div>

                                                
                                                    
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        @endforeach
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
