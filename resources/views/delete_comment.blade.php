@extends('layouts.app')

@section('content')
    <div class="container">
    <form method="POST" action="/comments/{{$comment->id}}">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Are you sure you want to delete this comment?
                                </div>
                                
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <div class="card-body">
                                    {!! nl2br(e($comment->content)) !!}
                                    </div>
                                    <a href="/posts/{{$comment->post->id}}" class="btn btn-light" style="display: inline-block">Cancel</a>
                                    </button>
                                    <button class="btn btn-danger" style="display: inline-block">Delete Comment
                                    </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
@endsection
