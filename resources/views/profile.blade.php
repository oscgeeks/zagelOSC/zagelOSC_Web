@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Profile
                                </div>
                                <div class="card-body">
                                    <p class="card-text">Name : {{$data->name}}</p>
                                    <p class="card-text">Email : {{$data->email}}</p>
                                    <p class="card-text">Created Date : {{$data->created_at}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
