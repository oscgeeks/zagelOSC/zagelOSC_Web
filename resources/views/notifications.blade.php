@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Notifications
                                </div>
                                <form method="POST" action="/notifications">
                                    @csrf
                                    <button class="btn btn-primary" style="display: block; margin: 0 auto; margin-top: 10px; margin-bottom: 10px;">
                                        Mark all as read
                                    </button>
                                </form>

                                @foreach (Auth::user()->notifications as $notification)
                                    @if($notification->type == "App\\Notifications\\NewReaction")
                                        <a class="dropdown-item"
                                           href="/posts/{{$notification->data['reactable_id']}}">
                                            {{ $notification->data["reactor"] }} reacted {{ $notification->data["reaction"] }} on your {{ $notification->data["reaction_on"] }}
                                        </a>
                                    @else
                                        <a class="dropdown-item" href="/posts/{{$notification->data['post_id']}}">
                                            Comments by {{ $notification->data["commenter"] }} and possibly others.
                                            <div align="right">{{ $notification->created_at }}</div>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
