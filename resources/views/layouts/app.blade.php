<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'zagelOSC') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<script src="{{ asset('js/align_text.js') }}"></script>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'zagelOSC') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="dropdown-item" href="{{ url('/') }}">
                                            Home
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="/users/{{ Auth::user()->id }}">
                                            Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ count(Auth::user()->unreadNotifications) }} Notifications <span
                                            class="caret"></span>
                                </a>
                                <?php $notifications = Auth::user()->notifications ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        @for ($i = 0; $i < min([count($notifications), 10]); $i++)
                                            @if($notifications[$i]->type == "App\\Notifications\\NewReaction")
                                            <a class="dropdown-item"
                                               href="/posts/{{$notifications[$i]->data['reactable_id']}}">
                                                {{ $notifications[$i]->data["reactor"] }} reacted {{ $notifications[$i]->data["reaction"] }} on your {{ $notifications[$i]->data["reaction_on"] }}
                                            </a>
                                            @else
                                            <a class="dropdown-item"
                                               href="/posts/{{$notifications[$i]->data['post_id']}}">
                                                Comments by {{ $notifications[$i]->data["commenter"] }} and possibly
                                                others
                                            </a>
                                            @endif
                                        @endfor
                                    <form method="POST" action="/notifications">
                                        @csrf
                                        <button class="btn btn-primary" style="float:left; margin-left: 10px; margin-top: 10px">
                                            Mark all as read
                                        </button>

                                        <button class="btn btn-primary" style="float:right; margin-right: 10px; margin-top: 10px" type="button"
                                                onclick="window.location='{{ url("/notifications") }}'">
                                            See All Notifications
                                        </button>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/users">Users</a>
                            </li>
                            @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
