@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="/comments/{{$comment->id}}">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Edit Comment
                                </div>
                                
                                    @csrf
                                    <input type="hidden" name="_method" value="PUT">
                                    <textarea class="form-control" rows="5" name="content" required>{{ $comment->content }}</textarea>
                                    <a href="/posts/{{$comment->post->id}}" class="btn btn-light" style="display: inline-block">Cancel</a>
                                    </button>
                                    <button class="btn btn-primary" style="display: inline-block">Edit Comment
                                    </button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
@endsection
