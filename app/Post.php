<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Hkp22\Laravel\Reactions\Traits\Reactable;
use Hkp22\Laravel\Reactions\Contracts\ReactableInterface;

class Post extends Model implements ReactableInterface
{
	use Reactable;

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getReactionsArray()
    {
	    $reactions = [
            "love"  => 0,
            "wow"   => 0,
            "angry" => 0,
            "sad"   => 0,
            "lol"   => 0
        ];

	    foreach ($this->reactionSummary()->toArray() as $reaction)
	    {
	        $reactions[$reaction['type']] = $reaction['count'];
	    }

	    return $reactions;
    }

    public function image_path()
    {
        $filename = $this->id . '.jpg';
        $path = storage_path() . '/app/post_images/' . $filename;

        if(!File::exists($path))
            return null;

        return '/post_images/' . $filename;
    }
}
