<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewReaction;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Post as PostResource;

use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    private $supported_reactions;

    public function __construct()
    {
        $this->supported_reactions = collect([
            "love",
            "wow",
            "angry",
            "sad",
            "lol"
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(15);

        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'image' => 'image',
        ]);

        $user = Auth::user();

        $post = new Post;

        $post->content = $request->input('content');
        $post->user_id = $user->id;

        if ($post->save())
        {
            if($request->hasFile('image'))
            {
                if(!$request->file('image')->isValid())
                {
                    return response("upload is not valid");
                }

                $image = $request->file('image');

                $original_image_filename = $post->id . '.' . $image->getClientOriginalExtension();
                
                $image->storeAs('post_images', $original_image_filename);

                $resized_img = Image::make(storage_path('app/post_images/' . $original_image_filename));

                if($resized_img->width() > 1000)
                {
                    $resized_img->widen(1000);
                }

                $resized_img->save(storage_path('app/post_images/' . $post->id . '.jpg', 75));

                return redirect('home');
            }

            return redirect('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('post', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);

        $user = Auth::user();

        if($user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        return view('edit_post', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $post = Post::findOrFail($request->id);

        if($user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        $post->content = $request->input('content');
        $post->user_id = $user->id;

        if ($post->save()) {
            return redirect('posts/'.$post->id);
        }
    }

    public function delete(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $user = Auth::user();

        if($user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        return view('delete_post', ['post' => $post]);
    }

    public function react(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $reaction = $request->input('reaction_type');

        if(!$this->supported_reactions->contains($reaction))
        {
            return response('Unsupported Reaction.', 400);
        }

        $post->toggleReaction($reaction);

        if(Auth::user()->id != $post->user->id)
            Notification::send($post->user, new NewReaction($post, Auth::user(), $reaction));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        $user = Auth::user();

        if($user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        $filename = $post->id . '.jpg';
        Storage::delete('post_images/' . $filename);

        if ($post->delete()) {
            return redirect('home');
        }
    }
}
