<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index(Request $request)
	{
		return view('notifications');
	}

	public function markAsRead(Request $request)
	{
		$user = Auth::user();

		foreach ($user->unreadNotifications as $notification)
		    $notification->markAsRead();

		return back();
	}

}
