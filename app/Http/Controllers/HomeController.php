<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->simplePaginate(15);
        return view('home', ['posts' => $posts]);
    }

    public function all_users()
    {
        $users = User::all();

        return view('users', ['users' => $users]);
    }

    public function profile($id)
    {
        $data = User::findOrFail($id);

        return view('profile', ['data' => $data]);
    }
}
