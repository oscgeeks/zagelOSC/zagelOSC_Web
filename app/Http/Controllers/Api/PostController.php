<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewReaction;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Post as PostResource;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(15);

        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $post = $request->isMethod('put') ? Post::findOrFail($request->id) : new Post;

        if($request->isMethod('put') && $user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        $post->id = $request->input('id');
        $post->content = $request->input('content');
        $post->user_id = $user->id;

        if ($post->save()) {
            return new PostResource($post);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return new PostResource($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function react(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $reaction = $request->input('reaction_type');

        if(!$this->supported_reactions->contains($reaction))
        {
            return response('Unsupported Reaction.', 400);
        }

        $post->toggleReaction($reaction);

        if(Auth::user()->id != $post->user->id)
            Notification::send($post->user, new NewReaction($post, Auth::user(), $reaction));
        
        return response(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        $user = Auth::user();

        if($user->id != $post->user->id)
        {
            return response('Unauthorised.', 401);
        }

        if ($post->delete()) {
            return new PostResource($post);
        }
    }
}
