<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function index(Request $request)
	{
		$user = Auth::user();

		$notifications = $user->notifications()->orderBy('updated_at', 'desc')->paginate(15);

		return response()->json(['notifications' => $notifications]);
	}

	public function markAsRead(Request $request)
	{
		$user = Auth::user();

		foreach ($user->unreadNotifications as $notification)
		    $notification->markAsRead();

		return response()->json("read");
	}

}
