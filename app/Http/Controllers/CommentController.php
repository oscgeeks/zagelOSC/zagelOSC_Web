<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Collection;
use App\Http\Requests;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Http\Resources\Comment as CommentResource;
use App\Notifications\NewComment;
use App\Notifications\NewReaction;

use Intervention\Image\Facades\Image;

class CommentController extends Controller
{
    private $supported_reactions;

    public function __construct()
    {
        $this->supported_reactions = collect([
            "love",
            "wow",
            "angry",
            "sad",
            "lol"
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

        return CommentResource::collection($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Gets all the users involved in a post whether it is
     * the post auther or any of the commenters. But we
     * don't want to return the current user because
     * he would then receive a notification for an
     * an action that he himself made. Which is
     * stupid. :D
     *
     * @param  \App\Post  $post
     * @param  \App\User  $me
     * @return Illuminate\Support\Collection of \App\User
     */
    private function getAllUsersInvolvedExceptForMe(Post $post, User $me)
    {
        $users_involved = collect([]);

        if($post->user->id != $me->id)
            $users_involved->push($post->user);

        foreach ($post->comments as $comment)
        {
            if($comment->user->id != $me->id)
                $users_involved->push($comment->user);
        }

        return $users_involved->unique();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'image' => 'image',
        ]);

        $user = Auth::user();

        $comment = new Comment;

        $comment->content = $request->input('content');
        $comment->post_id = $request->input('post_id');
        $comment->user_id = $user->id;


        if ($comment->save())
        {
            $all_users_involved = $this->getAllUsersInvolvedExceptForMe($comment->post, $user);

            Notification::send($all_users_involved, new NewComment($comment));

            if($request->hasFile('image'))
            {
                if(!$request->file('image')->isValid())
                {
                    return response("upload is not valid");
                }

                $image = $request->file('image');

                $original_image_filename = $comment->id . '.' . $image->getClientOriginalExtension();
                
                $image->storeAs('comment_images', $original_image_filename);

                $resized_img = Image::make(storage_path('app/comment_images/' . $original_image_filename));

                if($resized_img->width() > 1000)
                {
                    $resized_img->widen(1000);
                }

                $resized_img->save(storage_path('app/comment_images/' . $comment->id . '.jpg', 75));
            }

            return redirect('/posts/' . $comment->post_id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);

        return new CommentResource($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::findOrFail($id);

        $user = Auth::user();

        if($user->id != $comment->user->id)
        {
            return response('Unauthorised.', 401);
        }

        return view('edit_comment', ['comment' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $comment = Comment::findOrFail($request->id);
        
        if($user->id != $comment->user->id)
        {
            return response('Unauthorised.', 401);
        }

        $comment->content = $request->input('content');

        if ($comment->save()) {

            return redirect('/posts/'.$comment->post_id);
        }
    }

    public function delete(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        $user = Auth::user();

        if($user->id != $comment->user->id)
        {
            return response('Unauthorised.', 401);
        }

        return view('delete_comment', ['comment' => $comment]);
    }

    public function react(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $reaction = $request->input('reaction_type');

        if(!$this->supported_reactions->contains($reaction))
        {
            return response('Unsupported Reaction.', 400);
        }

        $comment->toggleReaction($reaction);

        if(Auth::user()->id != $comment->user->id)
            Notification::send($comment->user, new NewReaction($comment, Auth::user(), $reaction));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        $user = Auth::user();

        if($user->id != $comment->user->id)
        {
            return response('Unauthorised.', 401);
        }

        $post = $comment->post;

        if ($comment->delete()) {
            return redirect('/posts/'.$post->id);
        }
    }
}
