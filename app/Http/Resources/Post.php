<?php

namespace App\Http\Resources;

use App\Comment;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Comment as CommentResource;

class Post extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'user' => $this->user,
            'reactions' => $this->getReactionsArray(),
            'comments' => CommentResource::collection($this->comments),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    public function with($request) {
        return [
            'version' => '1.0.0',
            'author_url' => url('http://www.oscgeeks.org')
        ];
    }
}
