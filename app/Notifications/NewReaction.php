<?php

namespace App\Notifications;

use App\User;
use App\Post;
use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewReaction extends Notification
{
    use Queueable;

    protected $reaction_type;
    protected $reactable;
    protected $reactor;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($comment_or_post, User $reactor, $reaction_type)
    {
        $this->reactable = $comment_or_post;
        $this->reactor = $reactor;
        $this->reaction_type = $reaction_type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $reaction_on = "";
        $id = null;

        if ($this->reactable instanceof Comment)
        {
            $reaction_on = "comment";
            $id = $this->reactable->post->id;
        }
        else
        {
            $reaction_on = "post";
            $id = $this->reactable->id;
        }

        return [
            'reaction_on' => $reaction_on,
            'reactable_id' => $id,
            'reactor' => $this->reactor->name,
            'reaction' => $this->reaction_type,
        ];
    }
}
