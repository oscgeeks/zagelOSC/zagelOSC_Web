<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Hkp22\Laravel\Reactions\Traits\Reactable;
use Hkp22\Laravel\Reactions\Contracts\ReactableInterface;

class Comment extends Model implements ReactableInterface
{
	use Reactable;

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getReactionsArray()
    {
	    $reactions = [
            "love"  => 0,
            "wow"   => 0,
            "angry" => 0,
            "sad"   => 0,
            "lol"   => 0
        ];

	    foreach ($this->reactionSummary()->toArray() as $reaction)
	    {
	        $reactions[$reaction['type']] = $reaction['count'];
	    }

	    return $reactions;
    }

    public function image_path()
    {
        $filename = $this->id . '.jpg';
        $path = storage_path() . '/app/comment_images/' . $filename;

        if(!File::exists($path))
            return null;

        return '/comment_images/' . $filename;
    }
}
