<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('users','HomeController@all_users');
	Route::get('users/{id}','HomeController@profile');

	Route::post('posts', 'PostController@store');
	Route::get ('posts/{id}', 'PostController@show');
	Route::post('comments', 'CommentController@store');

	Route::get('posts/{id}/edit', 'PostController@edit');
	Route::get('comments/{id}/edit', 'CommentController@edit');
	Route::get('posts/{id}/delete', 'PostController@delete');
	Route::get('comments/{id}/delete', 'CommentController@delete');

	Route::post('posts/{id}/react', 'PostController@react');
	Route::post('/comments/{id}/react', 'CommentController@react');

	Route::put('posts/{id}', 'PostController@update');
	Route::put('comments/{id}', 'CommentController@update');

	Route::delete('posts/{id}', 'PostController@destroy');
	Route::delete('comments/{id}', 'CommentController@destroy');

	Route::get('notifications', 'NotificationController@index');
	Route::post('notifications', 'NotificationController@markAsRead');

	Route::get('post_images/{filename}', function ($filename)
	{
	    $path = storage_path() . '/app/post_images/' . $filename;

	    if(!File::exists($path)) abort(404);

	    $file = File::get($path);
	    $type = File::mimeType($path);

	    $response = Response::make($file, 200);
	    $response->header("Content-Type", $type);
	    return $response;
	});

	Route::get('comment_images/{filename}', function ($filename)
	{
	    $path = storage_path() . '/app/comment_images/' . $filename;

	    if(!File::exists($path)) abort(404);

	    $file = File::get($path);
	    $type = File::mimeType($path);

	    $response = Response::make($file, 200);
	    $response->header("Content-Type", $type);
	    return $response;
	});

});