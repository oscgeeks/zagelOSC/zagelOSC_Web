<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Auth
Route::post('login', 'Api\PassportController@login');
Route::post('register', 'Api\PassportController@register');

Route::group(['middleware' => 'auth:api'], function(){

	Route::get('get-details', 'Api\PassportController@getDetails');

	Route::get('users', 'UserController@index');

	Route::get   ('posts', 	    'Api\PostController@index');
	Route::get   ('posts/{id}', 'Api\PostController@show');
	Route::post  ('posts', 		'Api\PostController@store');
	Route::put   ('posts', 		'Api\PostController@store');
	Route::delete('posts/{id}', 'Api\PostController@destroy');
	Route::post  ('posts/{id}/react', 'Api\PostController@react');

	Route::get   ('comments', 	   'Api\CommentController@index');
	Route::get   ('comments/{id}', 'Api\CommentController@show');
	Route::post  ('comments', 	   'Api\CommentController@store');
	Route::put   ('comments', 	   'Api\CommentController@store');
	Route::delete('comments/{id}', 'Api\CommentController@destroy');
	Route::post  ('/comments/{id}/react', 'Api\CommentController@react');

	Route::get ('notifications', 'Api\NotificationController@index');
	Route::post('notifications', 'Api\NotificationController@markAsRead');
});